package com.example.anray.buyingapp.di.modules;

import android.content.Context;

import com.example.anray.buyingapp.data.managers.PreferencesManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anray on 16.12.2016.
 */

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }

}
