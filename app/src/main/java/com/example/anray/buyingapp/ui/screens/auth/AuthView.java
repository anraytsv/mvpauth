package com.example.anray.buyingapp.ui.screens.auth;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.mvp.presenters.IAuthPresenter;
import com.example.anray.buyingapp.mvp.views.IAuthView;

import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;

/**
 * Created by anray on 06.01.2017.
 */

public class AuthView extends RelativeLayout implements IAuthView, TextWatcher {

    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;

    private final Pattern mEmailRegex = Pattern.compile("^[\\w]{3,}@[\\w]{2,}\\.[\\w]{2,3}$");
    private final Pattern mPasswordRegex = Pattern.compile("^.{8,}$");

    @BindView(R.id.show_catalogue_btn)
    Button mShowCatalogueBtn;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.login_email_et)
    EditText mLoginEt;

    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.login_btn)
    Button mLoginBtn;


    @Inject
    AuthScreen.AuthPresenter mPresenter;

    private AuthScreen mScreen;


    public AuthView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AuthScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @OnClick(R.id.logo_img)
    void test() {
        ObjectAnimator oa1 = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 2f);
        ObjectAnimator oa2 = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", -2f);
        ObjectAnimator oa3 = ObjectAnimator.ofFloat(mLoginBtn, "rotationY", 0f);

        AnimatorSet animatorSet = new AnimatorSet();

        animatorSet.playSequentially(oa1, oa2, oa3);
        animatorSet.setDuration(100);
        animatorSet.start();

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        if (!isInEditMode()) {
            showViewByState();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    private void showViewByState() {
        if (mScreen.getCurrentState() == LOGIN_STATE) {
            showLoginState();
        } else {
            showIdleState();
        }
    }

    private void showLoginState() {
        mAuthCard.setVisibility(VISIBLE);
        animate(true);
        mShowCatalogueBtn.setVisibility(GONE);

    }

    private void showIdleState() {
        animate(false);
    }

    public void animate(boolean in) {
     /*   LinearLayout dialog   = (LinearLayout)findViewById(R.id.auth_card);
        dialog.setVisibility(LinearLayout.VISIBLE);
        Animation animation   =    AnimationUtils.loadAnimation(getContext(), R.anim_in.anim_in);
        animation.setDuration(500);
        dialog.setAnimation(animation);
        dialog.animate();
        animation.start();*/
        if (in) {
            final Animation move_anim = AnimationUtils.loadAnimation(getContext(), R.anim.anim_in);
            mAuthCard.startAnimation(move_anim);
        } else {
            final Animation move_anim = AnimationUtils.loadAnimation(getContext(), R.anim.anim_out);
            move_anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mAuthCard.setVisibility(GONE);
                    mShowCatalogueBtn.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mAuthCard.startAnimation(move_anim);
        }

    }

    //region    ========================== Fields validation ==========================
    private boolean isValid(Editable s, Pattern regEx) {
        String result = s.toString().replaceAll(" ", "");
        return regEx.matcher(result).matches();
    }

    public boolean checkFieldsSetAndClearErrors() {
        boolean validFlag = false;

        if (!isValid(mPasswordEt.getText(), mPasswordRegex)) {
            showError(mPasswordEt, getContext().getResources().getString(R.string.error_message_short_password));
            //mPasswordEt.setError("Пароль короче 8 символов");
            mPasswordEt.addTextChangedListener(this);
            //mPasswordEt.requestFocus();
            validFlag = false;
        } else {
            hideError(mPasswordEt);
            //mPasswordEt.setError(null);
            mPasswordEt.removeTextChangedListener(this);
            validFlag = true;
        }

        if (!isValid(mLoginEt.getText(), mEmailRegex)) {
            showError(mLoginEt, getContext().getResources().getString(R.string.error_message_wrong_email));
            //mLoginEt.setError("Неверный email");
            mLoginEt.addTextChangedListener(this);
            //mLoginEt.requestFocus();
            validFlag = false;
        } else {
            hideError(mLoginEt);
            //mLoginEt.setError(null);
            mLoginEt.removeTextChangedListener(this);
            validFlag = true;
        }

        return validFlag;
    }

    @Override
    public void afterTextChanged(Editable input) {


        if (!isValid(mLoginEt.getText(), mEmailRegex)) {
            showError(mLoginEt, getContext().getResources().getString(R.string.error_message_wrong_email));
        } else {
            hideError(mLoginEt);
        }


        if (!isValid(mPasswordEt.getText(), mPasswordRegex)) {
            showError(mPasswordEt, getContext().getResources().getString(R.string.error_message_short_password));
        } else {
            hideError(mPasswordEt);
        }


    }


    private void showError(EditText et, String message) {
        ((TextInputLayout) et.getParent().getParent()).setErrorEnabled(true);
        ((TextInputLayout) et.getParent().getParent()).setError(message);
    }

    private void hideError(EditText et) {
        ((TextInputLayout) et.getParent().getParent()).setErrorEnabled(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }
    //endregion ========================== Fields validation ==========================


    //region    ========================== Events ==========================

    @OnClick(R.id.login_btn)
    void loginClick() {
        mPresenter.clickOnLogin();
    }

    @Override
    @OnClick(R.id.show_catalogue_btn)
    public void showCatalogScreen() {
        mPresenter.clickOnShowCatalog();
    }

    @OnClick(R.id.fb_btn)
    void fbClick() {
        mPresenter.clickOnFb();
    }

    @OnClick(R.id.vk_btn)
    void vkClick() {
        mPresenter.clickOnVk();
    }

    @OnClick(R.id.twitter_btn)
    void twitterClick() {
        mPresenter.clickOnTwitter();
    }


    //endregion ========================== Events ==========================

    //region    ========================== IAuthView ==========================

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(VISIBLE);

    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(GONE);

    }

    @Override
    public String getUserEmail() {
        return String.valueOf(mLoginEt.getText());
    }

    @Override
    public String getUserPassword() {
        return String.valueOf(mPasswordEt.getText());
    }

    @Override
    public boolean isIdle() {
        return mScreen.getCurrentState() == IDLE_STATE;
    }

    @Override
    public void setCurrentState(int state) {

        mScreen.setCurrentState(state);
        showViewByState();
    }

    @Override
    public boolean viewOnBackPressed() {

        if (!isIdle()) {
            setCurrentState(IDLE_STATE);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    //endregion ========================== IAuthView ==========================


}
