package com.example.anray.buyingapp.ui.screens.catalog;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.di.scopes.CatalogScope;
import com.example.anray.buyingapp.flow.AbstractScreen;
import com.example.anray.buyingapp.flow.Screen;
import com.example.anray.buyingapp.mvp.models.CatalogModel;
import com.example.anray.buyingapp.mvp.presenters.ICatalogPresenter;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;
import com.example.anray.buyingapp.mvp.views.IRootView;
import com.example.anray.buyingapp.ui.activities.RootActivity;
import com.example.anray.buyingapp.ui.screens.auth.AuthScreen;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.ViewPresenter;

/**
 * Created by anray on 16.01.2017.
 */

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen<RootActivity.RootComponent> {


    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return null;
    }


    //region    ========================== DI ==========================


    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogModel providesCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter providesCatalogPresenter() {
            return new CatalogPresenter();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = {Module.class})
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter presenter);

        void inject(CatalogView view);

        CatalogModel getCatalogModel();

        Picasso getPicasso();
    }

    //endregion ========================== DI ==========================


    //region    ========================== Presenter ==========================

    public class CatalogPresenter extends ViewPresenter<CatalogView> implements ICatalogPresenter {

        @Inject
        CatalogModel mCatalogModel;

        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                getView().showCatalogView(mCatalogModel.getProductList());
            }
        }

        //region    ========================== ICatalogPresenter ==========================

        @Override
        public void clickOnBuyButton(int position) {

            if (getView() != null) {

                if (checkUserAuth() && getRootView() != null) {
                    getRootView().showMessage("Товар " + mCatalogModel.getProductList().get(position).getProductName()
                            + " успешно добавлен в корзину.");
//                getView().showAddToCartMessage(mProductList.get(position));
                } else {
                    Flow.get(getView()).set(new AuthScreen());
                }

            }

        }

        @Override
        public boolean checkUserAuth() {
            return mCatalogModel.isUserAuth();
        }

        //endregion ========================== ICatalogPresenter ==========================

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

    }


    //endregion ========================== Presenter ==========================
}
