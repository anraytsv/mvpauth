package com.example.anray.buyingapp.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.di.scopes.CatalogScope;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.mvp.presenters.CatalogPresenter;
import com.example.anray.buyingapp.mvp.presenters.ICatalogPresenter;
import com.example.anray.buyingapp.mvp.views.ICatalogView;
import com.example.anray.buyingapp.ui.fragments.adapters.CatalogAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

/**
 * Created by anray on 02.11.2016.
 */

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {

    @Inject
    CatalogPresenter mPresenter;


    @BindView(R.id.add_to_cart_btn)
    Button addToCartBtn;

    @BindView(R.id.product_pager)
    ViewPager productPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);

        Component component = DaggerService.getComponent(Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class, component);
        }
        component.inject(this);

        mPresenter.takeView(this);
        mPresenter.initView();
        addToCartBtn.setOnClickListener(this);

        mPresenter.getRootView().setCurrentDrawerOption(R.id.drawer_menu_catalog);


        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //region ========================== ICatalogView ==========================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void showCatalogView(List<ProductDTO> productsList) {

        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());

        for (ProductDTO productDTO : productsList) {
            adapter.addItem(productDTO);
        }

        productPager.setAdapter(adapter);

    }


    /*@Override
    public void showAuthScreen() {

        FragmentTransaction fmt = getActivity().getSupportFragmentManager().beginTransaction();
        fmt.replace(R.id.fragment_container, new AuthFragment(), AuthFragment.class.getSimpleName());
        fmt.commit();

//        getRootActivity().hideToolbar();

        // TODO: 02.11.2016 show auth screen if user not auth

    }*/

    @Override
    public void updateProductCounter() {

        // TODO: 02.11.2016 update count product on cart icon 

    }

    @Override
    public ICatalogPresenter getPresenter() {
        return null;
    }



    //endregion

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_to_cart_btn) {
            mPresenter.clickOnBuyButton(productPager.getCurrentItem());
            mPresenter.checkUserAuth();
        }

    }

    //region    ========================== DI ==========================

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = CatalogFragment.Module.class)
    @CatalogScope
    public interface Component {

        void inject(CatalogFragment catalogFragment);
    }


    private Component createDaggerComponent() {

        return DaggerCatalogFragment_Component.builder()
                .module(new Module())
                .build();
    }

    //endregion ========================== DI ==========================

}
