package com.example.anray.buyingapp.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anray.buyingapp.MvpApplication;
import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.di.components.DaggerPicassoComponent;
import com.example.anray.buyingapp.di.components.PicassoComponent;
import com.example.anray.buyingapp.di.modules.PicassoCacheModule;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.di.scopes.ProductScope;
import com.example.anray.buyingapp.mvp.presenters.IProductPresenter;
import com.example.anray.buyingapp.mvp.presenters.ProductPresenter;
import com.example.anray.buyingapp.mvp.views.IProductView;
import com.example.anray.buyingapp.ui.activities.RootActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

/**
 * Created by anray on 01.11.2016.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {

    @BindView(R.id.product_title)
    TextView mProductTitle;

    @BindView(R.id.product_description)
    TextView mProductDescription;

    @BindView(R.id.product_image)
    ImageView mProductImageView;

    @BindView(R.id.product_quantity)
    TextView mProductQuantity;

    @BindView(R.id.product_price)
    TextView mProductPrice;

    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;

    // image placeholder
    @BindDrawable(R.drawable.product)
    Drawable mProductImage;

    private static final String PRODUCT = "PRODUCT";

    @Inject
    ProductPresenter mPresenter;

    @Inject
    Picasso mPicasso;
    public ProductDTO mProduct;

    public ProductFragment() {
    }

    public static ProductFragment newInstance(ProductDTO product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PRODUCT, product);
        ProductFragment frag = new ProductFragment();
        frag.setArguments(bundle);

        return frag;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            mProduct = bundle.getParcelable(PRODUCT);

            Component component = createDaggerComponent(mProduct);
            component.inject(this);



            // TODO: 17.12.2016 fix recreate component
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);

        readBundle(getArguments());

        mPresenter.takeView(this);
        mPresenter.initView();

        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);


        return view;
        //return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        mPresenter = null;
        super.onDestroyView();

    }


    //region ========================== IProductView ==========================


    @Override
    public void showProductView(final ProductDTO product) {

        mProductTitle.setText(product.getProductName());
        mProductDescription.setText(product.getDescription());
        mProductQuantity.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPrice.setText(product.getPrice() * product.getCount() + ".-");
        } else {
            mProductPrice.setText(product.getPrice() + ".-");
        }


        mPicasso
                .load(product.getImageUrl())
                .error(mProductImage)
                .placeholder(mProductImage)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mProductImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                        mPicasso
                                .load(product.getImageUrl())
                                .error(mProductImage)
                                .placeholder(mProductImage)
                                .fit()
                                .centerCrop()
                                .into(mProductImageView);

                    }
                });
        //mProductImageView.setImageDrawable(mProductImage);

    }

    @Override
    public void updateProductCountView(ProductDTO product) {

        mProductQuantity.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPrice.setText(product.getPrice() * product.getCount() + ".-");
        }
    }

    //endregion


    //region ========================== IView ==========================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

/*

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
*/


    //endregion


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();

                break;

        }

    }

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public IProductPresenter getPresenter() {
        return null;
    }



    //region    ========================== DI ==========================

    @dagger.Module
    public class Module {

        ProductDTO mProductDto;

        public Module(ProductDTO productDTO) {
            mProductDto = productDTO;

        }

        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter(mProductDto);
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = {Module.class})
    @ProductScope
    public interface Component {

        void inject(ProductFragment productFragment);

    }

    public Component createDaggerComponent(ProductDTO productDTO) {
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if (picassoComponent == null) {
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(MvpApplication.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registerComponent(PicassoComponent.class,picassoComponent);
        }

      return DaggerProductFragment_Component.builder()
                    .picassoComponent(picassoComponent)
                    .module(new Module(productDTO))
                    .build();
    }

    //endregion ========================== DI ==========================

}
