package com.example.anray.buyingapp.mvp.views;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.mvp.presenters.IProductPresenter;

/**
 * Created by anray on 01.11.2016.
 */

public interface IProductView extends IView<IProductPresenter> {

  void  showProductView(ProductDTO product);
   void updateProductCountView(ProductDTO product);

}
