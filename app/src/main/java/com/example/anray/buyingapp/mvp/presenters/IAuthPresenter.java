package com.example.anray.buyingapp.mvp.presenters;

/**
 * Created by anray on 23.10.2016.
 */

public interface IAuthPresenter extends IPresenter {

   /* void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();*/

    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTwitter();
    void clickOnShowCatalog();

    boolean checkUserAuth();



}
