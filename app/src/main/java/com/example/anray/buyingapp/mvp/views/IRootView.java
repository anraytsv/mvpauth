package com.example.anray.buyingapp.mvp.views;

import android.support.annotation.IdRes;
import android.support.annotation.Nullable;

import com.example.anray.buyingapp.mvp.presenters.IPresenter;

/**
 * Created by anray on 17.12.2016.
 */
public interface IRootView<T extends IPresenter> extends IView<T> {

    @Nullable
    IView getCurrentScreen();

    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();

    void showAuthScreen();

    void showCatalogScreen();

    void showAccountScreen();

    void lockDrawer();

    void unLockDrawer();

    void setCurrentDrawerOption(@IdRes int resId);

    void showToolbar();

    void hideToolbar();

    void updateBasketCounter(int quantity);

    void showBasket();

    void hideBasket();

    void hideKeyboard();

}
