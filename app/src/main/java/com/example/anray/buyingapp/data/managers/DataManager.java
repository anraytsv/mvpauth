package com.example.anray.buyingapp.data.managers;

import com.example.anray.buyingapp.MvpApplication;
import com.example.anray.buyingapp.data.network.RestService;
import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.di.components.DaggerDataManagerComponent;
import com.example.anray.buyingapp.di.components.DataManagerComponent;
import com.example.anray.buyingapp.di.modules.LocalModule;
import com.example.anray.buyingapp.di.modules.NetworkModule;
import com.example.anray.buyingapp.di.scopes.DaggerService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by anray on 23.10.2016.
 */

public class DataManager {

    @Inject
    PreferencesManager mPreferencesManager;

    @Inject
    RestService mRestService;

    private List<ProductDTO> mProductList;


    public DataManager() {

        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if (component == null) {
           component = createComponent();
            DaggerService.registerComponent(DataManagerComponent.class,component);
        }
        component.inject(this);

//        new PicassoCache(mContext);
        generateMockData();
    }

    private DataManagerComponent createComponent() {

        return  DaggerDataManagerComponent.builder()
                 .appComponent(MvpApplication.getAppComponent())
                 .localModule(new LocalModule())
                 .networkModule(new NetworkModule())
                 .build();
    }


    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }


    public ProductDTO getProductById(int id) {
        // TODO: 01.11.2016 Get product from DataManager from Db
        return mProductList.get(id - 1);
    }

    public void updateProduct(ProductDTO product) {

        // TODO: 01.11.2016 Update product in DB
    }

    public List<ProductDTO> getProductList() {
        // TODO: 01.11.2016 get product list from anywhere
        return mProductList;
    }

    public void generateMockData() {
        mProductList = new ArrayList();
        mProductList.add(new ProductDTO(1, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGENGolfPlus-3783_7.jpg", "description 1 description 1 description 1 description 1 description 1", 100, 1));
        mProductList.add(new ProductDTO(2, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGENGolfPlus-3783_8.jpg", "description 1 description 1 description 1 description 1 description 1", 200, 1));
        mProductList.add(new ProductDTO(3, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGENGolfPlus-3783_9.jpg", "description 1 description 1 description 1 description 1 description 1", 300, 1));
        mProductList.add(new ProductDTO(4, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGENGolfPlus-3783_10.jpg", "description 1 description 1 description 1 description 1 description 1", 400, 1));
        mProductList.add(new ProductDTO(5, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGENGolfPlus-3783_11.jpg", "description 1 description 1 description 1 description 1 description 1", 500, 1));
        mProductList.add(new ProductDTO(6, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_17.jpg", "description 1 description 1 description 1 description 1 description 1", 600, 1));
        mProductList.add(new ProductDTO(7, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_18.jpg", "description 1 description 1 description 1 description 1 description 1", 700, 1));
        mProductList.add(new ProductDTO(8, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_19.jpg", "description 1 description 1 description 1 description 1 description 1", 800, 1));
        mProductList.add(new ProductDTO(9, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_20.jpg", "description 1 description 1 description 1 description 1 description 1", 900, 1));
        mProductList.add(new ProductDTO(10, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_23.jpg", "description 1 description 1 description 1 description 1 description 1", 1000, 1));
        mProductList.add(new ProductDTO(11, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_24.jpg", "description 1 description 1 description 1 description 1 description 1", 1100, 1));
        mProductList.add(new ProductDTO(12, "BestProduct", "http://s1.cdn.autoevolution.com/images/gallery/VOLKSWAGEN-Golf-Plus-3783_27.jpg", "description 1 description 1 description 1 description 1 description 1", 1200, 1));

    }


    public boolean isAuthUser() {

     /*   if (mPreferencesManager.getToken().equals("")) {
            return false;
        }*/
        return true;
    }

    public static DataManager getInstance() {
        return new DataManager();
    }
}
