package com.example.anray.buyingapp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.mvp.presenters.ICatalogPresenter;
import com.example.anray.buyingapp.mvp.views.ICatalogView;
import com.example.anray.buyingapp.ui.fragments.adapters.CatalogAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;

/**
 * Created by anray on 16.01.2017.
 */

public class CatalogView extends RelativeLayout implements ICatalogView {


    @BindView(R.id.add_to_cart_btn)
    Button addToCartBtn;

    @BindView(R.id.product_pager)
    ViewPager productPager;

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;

    private CatalogScreen mScreen;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mScreen = Flow.getKey(this);
//        DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //region    ========================== ICatalogView ==========================

    @Override
    public void showCatalogView(List<ProductDTO> productsList) {


        CatalogAdapter adapter = null;// new CatalogAdapter(getChildFragmentManager());

        for (ProductDTO productDTO : productsList) {
            adapter.addItem(productDTO);
        }

        productPager.setAdapter(adapter);
    }

    @Override
    public void updateProductCounter() {

    }

    //endregion ========================== ICatalogView ==========================


    //region    ========================== IView ==========================

    @Override
    public ICatalogPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion ========================== IView ==========================


}
