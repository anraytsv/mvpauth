package com.example.anray.buyingapp.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anray on 16.12.2016.
 */


@Module
public class AppModule {

    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
