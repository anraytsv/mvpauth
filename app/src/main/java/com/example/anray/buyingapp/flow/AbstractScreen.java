package com.example.anray.buyingapp.flow;

import android.util.Log;

import com.example.anray.buyingapp.mortar.ScreenScoper;

import flow.ClassKey;

/**
 * Created by anray on 02.01.2017.
 */

public abstract class AbstractScreen<T> extends ClassKey {
    private static final String TAG = "AbstractScreen";

    public String getScopeName(){
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope(){
        Log.d(TAG,"unregisterScope " + this.getScopeName());
        ScreenScoper.destroyScreenScope(getScopeName());
    }
}
