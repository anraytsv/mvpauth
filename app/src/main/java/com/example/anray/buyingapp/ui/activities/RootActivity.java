package com.example.anray.buyingapp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.anray.buyingapp.BuildConfig;
import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.di.components.AppComponent;
import com.example.anray.buyingapp.di.modules.PicassoCacheModule;
import com.example.anray.buyingapp.di.modules.RootModule;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.di.scopes.RootScope;
import com.example.anray.buyingapp.flow.TreeKeyDispatcher;
import com.example.anray.buyingapp.mortar.ScreenScoper;
import com.example.anray.buyingapp.mvp.presenters.IRootPresenter;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;
import com.example.anray.buyingapp.mvp.views.IRootView;
import com.example.anray.buyingapp.mvp.views.IView;
import com.example.anray.buyingapp.ui.fragments.AuthFragment;
import com.example.anray.buyingapp.ui.screens.auth.AuthScreen;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

public class RootActivity extends AppCompatActivity implements IRootView<IRootPresenter>, NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = getClass().getSimpleName();
    private ProgressDialog mProgressDialog;


    @BindView(R.id.drawer)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.drawer_menu)
    NavigationView mDrawer;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.coordinator_root)
    CoordinatorLayout mCoordinator;

    @BindView(R.id.root_container)
    FrameLayout mViewsContainer;

    @BindView(R.id.appbar)
    AppBarLayout mAppbar;


    FragmentManager mFragmentManager;
    public AlertDialog mAlerDialog;

    @Inject
    RootPresenter mRootPresenter;


    //region ================= Life Cycle


    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new AuthScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();

        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(String name) {

        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        ButterKnife.bind(this);


        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);


//        mRootPresenter.takeView(this);
        mRootPresenter.initView();
        // TODO: 17.12.2016 init view

        initToolbar();
        initDrawer();

        mFragmentManager = getSupportFragmentManager();

      /*  if (savedInstanceState == null) {

            if (mRootPresenter.checkUserAuth()) {
                showCatalogScreen();
            } else {
                showAuthScreen();
            }
        }*/

        if (isCurrentFragment(AuthFragment.class)) {
            hideToolbar();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mRootPresenter.takeView(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mRootPresenter.dropView();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            Log.d(TAG, "Removing " + this.getClass().getSimpleName());
            ScreenScoper.destroyScreenScope(AuthScreen.class.getName());
//            DaggerService.unregisterScope(RootScope.class);
        }
        super.onDestroy();
    }


    //endregion

    //region    ========================== Activity methods ==========================

    @Override
    public void onBackPressed() {

        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            super.onBackPressed();
        }

       /* if (mDrawerLayout.isDrawerVisible(mDrawer)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        if (isCurrentFragment(AuthFragment.class)) {

            AuthPanel authPanel = findFragmentByTag(AuthFragment.class).getAuthPanel();
            if (!authPanel.isIdle()) {
                authPanel.setCurrentState(AuthPanel.IDLE_STATE);
            } else {
                super.onBackPressed();
            }
        } else {
            if (mAlerDialog == null || !mAlerDialog.isShowing()) {
                showAlertDialogOneButton(this, R.string.dialog_title_exit_app, R.string.dialog_message_exit_app, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mRootPresenter.logoutUser();
                        showAuthScreen();
                    }
                });
            } else {
                super.onBackPressed();
            }
        }*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_basket, menu);

        MenuItem item = menu.findItem(R.id.basket);
        MenuItemCompat.setActionView(item, R.layout.menu_basket_layout);


        RelativeLayout basketLayout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView tv = (TextView) basketLayout.findViewById(R.id.toolbar_number_in_basket);
//        tv.setText("12");


        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (findFragmentByTag(AuthFragment.class) == null) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }

        //return super.onMenuOpened(featureId, menu);
        return true;
    }

    //endregion ========================== Activity methods ==========================

    //region    ========================== IView ==========================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }


    @Override
    public IRootPresenter getPresenter() {
        return mRootPresenter;
    }


    //endregion ========================== IView ==========================

    //region ========================== IRootView ==========================


    @Override
    public void showAuthScreen() {

    }

    @Override
    public void showCatalogScreen() {

    }

    @Override
    public void showAccountScreen() {

    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) mViewsContainer.getChildAt(0);
    }

    @Override
    public void lockDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unLockDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void setCurrentDrawerOption(@IdRes int resId) {
        getDrawer().getMenu().findItem(resId).setChecked(true);
    }

    @Override
    public void showToolbar() {

        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }

        // add inset for fragment
//        mViewsContainer.setFitsSystemWindows(false);


        // Calculate ActionBar height
        int actionBarHeight = 0;

        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mViewsContainer.getLayoutParams();
        layoutParams.setMargins(0, actionBarHeight, 0, 0);

        mViewsContainer.setLayoutParams(layoutParams);

    }

    @Override
    public void hideToolbar() {

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mViewsContainer.getLayoutParams();
        layoutParams.setMargins(0, 0, 0, 0);

        mViewsContainer.setLayoutParams(layoutParams);

        //remove inset for fragment
        mViewsContainer.setFitsSystemWindows(true);

    }

    @Override
    public void updateBasketCounter(int quantity) {

    }

    @Override
    public void showBasket() {

    }

    @Override
    public void hideBasket() {

    }

    @Override
    public void hideKeyboard() {
        // Check if any view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinator, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.something_went_wrong));
            // TODO: 23.10.2016 send error details to crashlytics
        }
    }

    @Override
    public void showLoad() {

        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, null);
            if (mProgressDialog.getWindow() != null) {
                mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            mProgressDialog.setContentView(new ProgressBar(this));
            mProgressDialog.show();
        }
    }

    @Override
    public void hideLoad() {

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    //endregion

    //region    ========================== Other methods ==========================

    private void initDrawer() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mDrawer.setNavigationItemSelectedListener(this);

    }

    private void initToolbar() {

        setSupportActionBar(mToolbar);
    }

    public NavigationView getDrawer() {
        return mDrawer;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public <T extends Fragment> T findFragmentByTag(Class<T> fragmentClass) {
        return (T) mFragmentManager.findFragmentByTag(fragmentClass.getSimpleName());
    }

    private <T extends Fragment> boolean isCurrentFragment(Class<T> fragmentClass) {
        T fragment = findFragmentByTag(fragmentClass);

        return fragment != null && fragment.isVisible();
    }

    //endregion ========================== Other methods ==========================


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.drawer_menu_account:

                showAccountScreen();
//                fragment = new AccountFragment();
//                item.setChecked(true);
                break;
            case R.id.drawer_menu_catalog:

                showCatalogScreen();

                break;
            case R.id.drawer_menu_favorite:

                break;
            case R.id.drawer_menu_orders:

                break;
            case R.id.drawer_menu_notification:

                break;
        }


        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;


    }


    public void showAlertDialogOneButton(Context context, int title, int message, DialogInterface.OnClickListener onClickListener) {

        mAlerDialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok_btn, onClickListener)
                .create();

        mAlerDialog.show();


    }


    //region    ========================== DI ==========================


    @dagger.Component(dependencies = AppComponent.class, modules = {RootModule.class, PicassoCacheModule.class})
    @RootScope
    public interface RootComponent {

        void inject(RootActivity rootActivity);

        RootPresenter getRootPresenter();

        Picasso getPicasso();
    }


//endregion ========================== DI ==========================

}
