package com.example.anray.buyingapp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by anray on 23.10.2016.
 */

public class PreferencesManager {

    public static final String TOKEN = "TOKEN";
    private SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public String getToken() {
        return mSharedPreferences.getString(PreferencesManager.TOKEN, "");
    }

    public void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PreferencesManager.TOKEN, token);
        editor.apply();

    }

    public void clearToken() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PreferencesManager.TOKEN, null);
        editor.apply();

    }

}
