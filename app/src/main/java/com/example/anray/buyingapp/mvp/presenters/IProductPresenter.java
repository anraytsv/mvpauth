package com.example.anray.buyingapp.mvp.presenters;

/**
 * Created by anray on 01.11.2016.
 */

public interface IProductPresenter extends IPresenter {

    void clickOnPlus();

    void clickOnMinus();

}
