package com.example.anray.buyingapp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.anray.buyingapp.BuildConfig;
import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.mvp.presenters.IAuthPresenter;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;
import com.example.anray.buyingapp.mvp.views.IAuthView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {
    @BindView(R.id.coordinator_root)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.show_catalogue_btn)
    Button mShowCatalogueBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.auth_wrapper)
    LinearLayout mAuthPanel;

    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;
    @BindView(R.id.vk_btn) ImageButton mVkBtn;
    @BindView(R.id.twitter_btn) ImageButton mTwitterBtn;

    ProgressDialog mProgressDialog;

//    AuthPresenter mPresenter;
    RootPresenter mPresenter;
//region ================= Life Cycle


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

       /* mPresenter.takeView(this);
        mPresenter.initView();*/


        mShowCatalogueBtn.setOnClickListener(this);
        mLoginBtn.setOnClickListener(this);
        mFbBtn.setOnClickListener(this);
        mVkBtn.setOnClickListener(this);
        mTwitterBtn.setOnClickListener(this);

        mPasswordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
//                    mPresenter.clickOnLogin();
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        mPresenter.dropView();
    }

    //endregion


//region ================= IAuthView

    private void hideKeyboard() {

// Check if any view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

//    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

//    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.something_went_wrong));
            // TODO: 23.10.2016 send error details to crashlytics
        }
    }

//    @Override
    public void showLoad() {

        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, null);
            if (mProgressDialog.getWindow() != null) {
                mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            mProgressDialog.setContentView(new ProgressBar(this));
            mProgressDialog.show();
        }
    }

//    @Override
    public void hideLoad() {

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public IAuthPresenter getPresenter() {
        return null;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

   /* @Override
    public void testShowLoginCard() {
        mAuthCard.setVisibility(View.VISIBLE);
    }*/

  /*  @Override
    public LinearLayout getAuthPanel() {
        return mAuthPanel;
    }*/

//    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(this,RootActivity.class);
        startActivity(intent);
    }

    @Override
    public String getUserEmail() {
        return null;
    }

    @Override
    public String getUserPassword() {
        return null;
    }

    @Override
    public boolean isIdle() {
        return false;
    }

    @Override
    public void setCurrentState(int state) {

    }


    //endregion


    @Override
    public void onBackPressed() {

       /* if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCurrentState(AuthPanel.IDLE_STATE);
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalogue_btn:
//                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
//                mPresenter.clickOnLogin();
                break;
            case R.id.fb_btn:
//                mPresenter.clickOnFb();
                break;
            case R.id.vk_btn:
//                mPresenter.clickOnVk();
                break;
            case R.id.twitter_btn:
//                mPresenter.clickOnTwitter();
                break;
        }
    }

}
