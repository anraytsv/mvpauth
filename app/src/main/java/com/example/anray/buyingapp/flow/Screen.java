package com.example.anray.buyingapp.flow;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Scope;

/**
 * Created by anray on 02.01.2017.
 * создано вчера
 */

@Scope
@Retention(RetentionPolicy.RUNTIME) @Target(ElementType.TYPE)
public @interface Screen  {
    int value();
}
