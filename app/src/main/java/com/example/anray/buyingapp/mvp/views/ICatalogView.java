package com.example.anray.buyingapp.mvp.views;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.mvp.presenters.ICatalogPresenter;

import java.util.List;

/**
 * Created by anray on 02.11.2016.
 */

public interface ICatalogView extends IView<ICatalogPresenter> {

//    void showAddToCartMessage(ProductDTO productDTO);

    void showCatalogView(List<ProductDTO> productsList);

//    void showAuthScreen();

    void updateProductCounter();

}
