package com.example.anray.buyingapp.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.anray.buyingapp.mvp.views.IView;

/**
 * Created by anray on 01.11.2016.
 */

public abstract class AbstractPresenter<T extends IView> {

    final String TAG = getClass().getSimpleName();

    private T mView;

   public void takeView(T view){
       mView = view;
   }

    public  void dropView(){
        mView = null;
    }

    public abstract void initView();

    @Nullable
    public T getView(){
        return mView;
    }



}
