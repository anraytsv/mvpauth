package com.example.anray.buyingapp.mvp.presenters;

import android.support.annotation.Nullable;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.di.scopes.ProductScope;
import com.example.anray.buyingapp.mvp.models.ProductModel;
import com.example.anray.buyingapp.mvp.views.IProductView;

import javax.inject.Inject;

import dagger.Provides;

/**
 * Created by anray on 01.11.2016.
 */

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {
    private static final String TAG = "ProductPresenter";

    @Inject
    ProductModel mProductModel;
    private ProductDTO mProduct;

    public ProductPresenter(ProductDTO product) {

        Component component = DaggerService.getComponent(Component.class);
        if (component == null){
            component = createDaggerComponent();
            DaggerService.registerComponent(Component.class,component);
        }
        component.inject(this);

//        mProductModel = new ProductModel();
        mProduct = product;
    }

    public static ProductPresenter newInstance(ProductDTO product) {

        return new ProductPresenter(product);

    }

    @Override
    public void takeView(IProductView view) {
        super.takeView(view);
    }

    @Override
    public void dropView() {
        super.dropView();
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }

    }

    @Nullable
    @Override
    public IProductView getView() {
        return super.getView();
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mProductModel.updateProduct(mProduct);
        if (getView() != null) {
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {

        if (mProduct.getCount() > 0) {
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }

    }

    //region    ========================== DI ==========================

    @dagger.Module
    public class Module{

        @Provides
        @ProductScope
        ProductModel provideProductModel() {
            return new ProductModel();
        }
    }

    @dagger.Component(modules = ProductPresenter.Module.class)
    @ProductScope
    public interface Component{

        void inject(ProductPresenter productPresenter);
    }

    private Component createDaggerComponent(){

       return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    //endregion ========================== DI ==========================

}
