package com.example.anray.buyingapp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.ui.activities.RootActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_account, container, false);

        setCurrentDrawerOption();


        return view;
    }

    private void setCurrentDrawerOption() {
        ((RootActivity)getActivity()).getDrawer().getMenu().findItem(R.id.drawer_menu_account).setChecked(true);
    }

}
