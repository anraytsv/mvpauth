package com.example.anray.buyingapp.di.modules;

import com.example.anray.buyingapp.di.scopes.RootScope;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;

import dagger.Provides;

/**
 * Created by anray on 14.01.2017.
 */
@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
