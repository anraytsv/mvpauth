package com.example.anray.buyingapp.ui.fragments.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.ui.fragments.ProductFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anray on 02.11.2016.
 */

public class CatalogAdapter extends FragmentPagerAdapter {
    private List<ProductDTO> mProductList = new ArrayList<>();

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDTO product){
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
