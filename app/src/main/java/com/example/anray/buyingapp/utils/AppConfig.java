package com.example.anray.buyingapp.utils;

/**
 * Created by anray on 16.12.2016.
 */
public class AppConfig {
    public static final String BASE_URL = "http://test.com";
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
