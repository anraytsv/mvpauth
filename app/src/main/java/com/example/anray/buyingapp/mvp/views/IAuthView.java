package com.example.anray.buyingapp.mvp.views;

import com.example.anray.buyingapp.mvp.presenters.IAuthPresenter;

/**
 * Created by anray on 23.10.2016.
 */

public interface IAuthView extends IView<IAuthPresenter> {


    void showLoginBtn();
    void hideLoginBtn();

    void showCatalogScreen();

    String getUserEmail();

    String getUserPassword();

    boolean isIdle();

    void setCurrentState(int state);


}
