package com.example.anray.buyingapp.mvp.views;

import com.example.anray.buyingapp.mvp.presenters.IPresenter;

/**
 * Created by anray on 01.11.2016.
 */

public interface IView<T extends IPresenter> {

    T getPresenter();

    boolean viewOnBackPressed();



}
