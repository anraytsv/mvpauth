package com.example.anray.buyingapp.mvp.presenters;

/**
 * Created by anray on 02.11.2016.
 */

public interface ICatalogPresenter extends IPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
