package com.example.anray.buyingapp.mvp.presenters;

/**
 * Created by anray on 18.12.2016.
 */

public interface IRootPresenter extends IPresenter {

    void logoutUser();

    boolean checkUserAuth();
}
