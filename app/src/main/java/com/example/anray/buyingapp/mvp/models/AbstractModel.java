package com.example.anray.buyingapp.mvp.models;

import com.example.anray.buyingapp.data.managers.DataManager;
import com.example.anray.buyingapp.di.components.DaggerModelComponent;
import com.example.anray.buyingapp.di.components.ModelComponent;
import com.example.anray.buyingapp.di.modules.ModelModule;
import com.example.anray.buyingapp.di.scopes.DaggerService;

import javax.inject.Inject;

/**
 * Created by anray on 16.12.2016.
 */

public class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {

        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if (component == null) {
            component = createComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    private ModelComponent createComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
