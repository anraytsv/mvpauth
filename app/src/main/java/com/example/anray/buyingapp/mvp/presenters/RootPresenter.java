package com.example.anray.buyingapp.mvp.presenters;

import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.di.scopes.RootScope;
import com.example.anray.buyingapp.mvp.models.RootModel;
import com.example.anray.buyingapp.mvp.views.IRootView;

import javax.inject.Inject;

import dagger.Provides;

/**
 * Created by anray on 17.12.2016.
 */
public class RootPresenter extends AbstractPresenter<IRootView> implements IRootPresenter {

    @Inject
    RootModel mRootModel;

    public RootPresenter() {
        RootPresenter.Component component = DaggerService.getComponent(RootPresenter.Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(RootPresenter.Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        // TODO: 17.12.2016 init drawer avatr + username + saved state
    }

    //region    ========================== IRootPresenter ==========================

    @Override
    public void logoutUser(){

        mRootModel.clearToken();

    }

    @Override
    public boolean checkUserAuth() {
        return mRootModel.isUserAuth();
    }


    //endregion ========================== IRootPresenter ==========================


    //region    ========================== DI ==========================

    @dagger.Module
    public class Module {

        @Provides
        @RootScope
        RootModel provideRootModel() {
            return new RootModel();
        }
    }

    @dagger.Component( modules = RootPresenter.Module.class)
    @RootScope
    public interface Component {

        void inject(RootPresenter catalogPresenter);
    }


    private RootPresenter.Component createDaggerComponent() {

        return DaggerRootPresenter_Component.builder()
                .module(new RootPresenter.Module())
                .build();
    }

    //endregion ========================== DI ==========================

}
