package com.example.anray.buyingapp.ui.utils;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.example.anray.buyingapp.R;

import java.util.regex.Pattern;

/**
 * Created by anray on 23.10.2016.
 */

public class EditTextValidator implements TextWatcher {


    private final Pattern mEmailRegex = Pattern.compile("^[\\w]{3,}@[\\w]{2,}\\.[\\w]{2,3}$");
    private final Pattern mPasswordRegex = Pattern.compile("^.{8,}$");
    private View mView;
    private String mErrorMessage;


    public EditTextValidator(View view, String error) {
        mView = view;
        mErrorMessage = error;
    }


    private boolean isValid(CharSequence s, Pattern regEx) {
        String result = s.toString().replaceAll(" ", "");
        return regEx.matcher(result).matches();
    }

    @Override
    public void afterTextChanged(Editable input) {

        switch (mView.getId()) {
            case R.id.login_email_et:
                if (!isValid(input, mEmailRegex)) {
                    ((TextInputLayout) mView.getParent()).setError(mErrorMessage);
                } else {
                    ((TextInputLayout) mView.getParent()).setError(null);
                }
                break;
            case R.id.login_password_et:
                if (!isValid(input, mPasswordRegex)) {
                    ((TextInputLayout) mView.getParent()).setError(mErrorMessage);
                } else {
                    ((TextInputLayout) mView.getParent()).setError(null);
                }
                break;
        }


    }

    //region beforeTextChanged, onTextChanged
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //mCursorPosition = start + after;
        Log.d("XXX-before-start", String.valueOf(start));
        Log.d("XXX-before-count", String.valueOf(count));
        Log.d("XXX-before-after", String.valueOf(after));

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        Log.d("XXX-on-start", String.valueOf(start));
        Log.d("XXX-on-before", String.valueOf(before));
        Log.d("XXX-on-count", String.valueOf(count));

    }
    //endregion
}

