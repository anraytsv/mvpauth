package com.example.anray.buyingapp.mvp.models;

import android.text.TextUtils;

import com.example.anray.buyingapp.data.managers.DataManager;

/**
 * Created by anray on 23.10.2016.
 */

public class AuthModel extends AbstractModel {

    public AuthModel() {
    }

    public boolean isAuthUser() {

        if (!TextUtils.isEmpty(mDataManager.getPreferencesManager().getToken())) {
            return true;
        }

        return false;
    }

    public void loginUser(String email, String password) {
        // TODO: 23.10.2016 send data to server

        String token = "";
        mDataManager.getPreferencesManager().saveToken(token);

    }


    public void loginUserByToken() {
        String token = mDataManager.getPreferencesManager().getToken();

        // TODO: 23.10.2016 send data to server

    }

}
