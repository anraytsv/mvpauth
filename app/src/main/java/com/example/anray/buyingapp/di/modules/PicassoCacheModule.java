package com.example.anray.buyingapp.di.modules;

import android.content.Context;

import com.example.anray.buyingapp.di.scopes.RootScope;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anray on 16.12.2016.
 */

@Module
public class PicassoCacheModule {

    @Provides
    @RootScope
    Picasso providePicasso(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context);
        Picasso picasso = new Picasso.Builder(context).downloader(okHttp3Downloader).indicatorsEnabled(true).build();

        Picasso.setSingletonInstance(picasso);
        return picasso;
    }
}
