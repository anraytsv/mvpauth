package com.example.anray.buyingapp.di.scopes;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by anray on 11.12.2016.
 */

public class DaggerService {

    public static final String SERVICE_NAME = "MY_DAGGER_SERVICE";
    private static String TAG = "DaggerService";
    private static Map<Class, Object> sComponentMap = new HashMap<>();


    @SuppressWarnings("unchecked")
    public static <T> T getDaggerComponent(Context context) {
        //noinspection ResourceType
        return (T) context.getSystemService(SERVICE_NAME);
    }

    // TODO: 14.01.2017 refactor to use reflection

    public static void registerComponent(Class componentClass, Object daggerComponent) {
        sComponentMap.put(componentClass, daggerComponent);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass) {

        Object component = sComponentMap.get(componentClass);
        return (T) component;
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation) {

        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Class, Object> entry = iterator.next();
            if (entry.getKey().isAnnotationPresent(scopeAnnotation)) {
                Log.d(TAG, "Unregister scope " + scopeAnnotation.getSimpleName());
                iterator.remove();
            }
        }

    }
}
