package com.example.anray.buyingapp.mortar;

import android.support.annotation.Nullable;
import android.util.Log;

import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.flow.AbstractScreen;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import mortar.MortarScope;

/**
 * Created by anray on 02.01.2017.
 */

public class ScreenScoper {
    private static final String TAG = "ScreenScoper";
    private static Map<String, MortarScope> sScopeMap = new HashMap<>();


    public static MortarScope getScreenScope(AbstractScreen screen){
        if (!sScopeMap.containsKey(screen.getScopeName())){
            Log.d(TAG,"getScreenScope: create new scope");
            return createScreenScope(screen);
        } else {
            Log.d(TAG,"getScreenScope: return existing scope");
            return sScopeMap.get(screen.getScopeName());

        }
    }

    public static void registerScope(MortarScope scope){
        sScopeMap.put(scope.getName(),scope);

    }

    public static void destroyScreenScope(String scopeName){
        MortarScope mortarScope = sScopeMap.get(scopeName);
        mortarScope.destroy();
        cleanScopeMap();

    }

    private static void cleanScopeMap(){

        Iterator<Map.Entry<String,MortarScope>> iterator = sScopeMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String,MortarScope> entry = iterator.next();
            if (entry.getValue().isDestroyed()){
                iterator.remove();
            }
        }

    }

    @Nullable
    private static String getParentScopeName(AbstractScreen screen){

        try {
            String generic = ((Class)((ParameterizedType) screen.getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getName();

            String parentScopeName = generic;

            if (parentScopeName.contains("$")){
                parentScopeName = parentScopeName.substring(0,generic.indexOf("$"));
            }
            return parentScopeName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MortarScope createScreenScope(AbstractScreen screen){

        Log.d(TAG,"createScreenScope: with name : " + screen.getScopeName());

        MortarScope parentScope = sScopeMap.get(getParentScopeName(screen));
        Object screenComponent = screen.createScreenComponent(parentScope.getService(DaggerService.SERVICE_NAME));

        MortarScope newScope = parentScope.buildChild()
                .withService(DaggerService.SERVICE_NAME,screenComponent)
                .build(screen.getScopeName());

        registerScope(newScope);
        return newScope;
    }
}
