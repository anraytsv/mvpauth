package com.example.anray.buyingapp.di.components;

import android.content.Context;

import com.example.anray.buyingapp.di.modules.AppModule;

import dagger.Component;

/**
 * Created by anray on 16.12.2016.
 */

@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();
}
