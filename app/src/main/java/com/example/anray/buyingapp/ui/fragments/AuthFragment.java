package com.example.anray.buyingapp.ui.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.di.scopes.AuthScope;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.mvp.presenters.IAuthPresenter;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;
import com.example.anray.buyingapp.mvp.views.IAuthView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AuthFragment extends Fragment implements IAuthView, View.OnClickListener {

    final String TAG = getClass().getSimpleName();

    @BindView(R.id.coordinator_root)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.show_catalogue_btn)
    Button mShowCatalogueBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.auth_card)
    CardView mAuthCard;

    @BindView(R.id.auth_wrapper)
    LinearLayout mAuthPanel;

    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;
    @BindView(R.id.twitter_btn)
    ImageButton mTwitterBtn;

    ProgressDialog mProgressDialog;

//    @Inject
    RootPresenter mPresenter;

    public ViewGroup mContainer;

    public AuthFragment() {
        // Required empty public constructor

    }

    //region    ========================== LifeCycle ==========================


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  RootComponent component = DaggerService.getComponent(RootComponent.class);

        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(RootComponent.class, component);
        }
        component.inject(this);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auth, container, false);

        mContainer = container;

        ButterKnife.bind(this, view);


       /* mPresenter.takeView(this);
        mPresenter.initView();*/


        mShowCatalogueBtn.setOnClickListener(this);
        mLoginBtn.setOnClickListener(this);
        mFbBtn.setOnClickListener(this);
        mVkBtn.setOnClickListener(this);
        mTwitterBtn.setOnClickListener(this);

        mPasswordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
             /*       mPresenter.clickOnLogin();
                    mPresenter.getRootView().hideKeyboard();*/
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
//        mPresenter.dropView();
        if(isRemoving()){
            Log.d(TAG, "Removing " + this.getClass().getSimpleName());
            DaggerService.unregisterScope(AuthScope.class);
        }

        super.onDestroyView();
    }

    //endregion ========================== LifeCycle ==========================


    //region ================= IAuthView

    @Override
    public IAuthPresenter getPresenter() {
//        return mPresenter;
        return null;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public void showCatalogScreen() {

    }

    @Override
    public String getUserEmail() {
        return null;
    }

    @Override
    public String getUserPassword() {
        return null;
    }

    @Override
    public boolean isIdle() {
        return false;
    }

    @Override
    public void setCurrentState(int state) {

    }

/*    @Override
    public LinearLayout getAuthPanel() {
        return mAuthPanel;
    }*/

    //endregion

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_catalogue_btn:
//                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
//                mPresenter.clickOnLogin();
                break;
            case R.id.fb_btn:
//                mPresenter.clickOnFb();
                break;
            case R.id.vk_btn:
//                mPresenter.clickOnVk();
                break;
            case R.id.twitter_btn:
//                mPresenter.clickOnTwitter();
                break;
        }
    }


    //region    ========================== DI ==========================

/*
    @dagger.RootModule
    public class RootModule {

        @Provides
        @AuthScope
        AuthPresenter providesAuthPresenter() {
            return new AuthPresenter();
        }
    }

    @dagger.RootComponent(modules = RootModule.class)
    @AuthScope
    public interface RootComponent {
        void inject(AuthFragment authFragment);
    }

    private RootComponent createDaggerComponent() {
        return DaggerAuthFragment_Component.builder()
                .module(new RootModule())
                .build();
    }*/

    //endregion ========================== DI ==========================

}
