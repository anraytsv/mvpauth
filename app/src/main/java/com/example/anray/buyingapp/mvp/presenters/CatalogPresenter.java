package com.example.anray.buyingapp.mvp.presenters;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;
import com.example.anray.buyingapp.di.scopes.CatalogScope;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.mvp.models.CatalogModel;
import com.example.anray.buyingapp.mvp.views.ICatalogView;
import com.example.anray.buyingapp.mvp.views.IRootView;
import com.example.anray.buyingapp.ui.activities.RootActivity;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

/**
 * Created by anray on 02.11.2016.
 */
public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
//    private static CatalogPresenter ourInstance = new CatalogPresenter();

    @Inject
    RootPresenter mRootPresenter;

    @Inject
    CatalogModel mCatalogModel;
    private List<ProductDTO> mProductList;

//    public static CatalogPresenter getInstance() {
//        return ourInstance;
//    }

    public CatalogPresenter() {
//        mCatalogModel = new CatalogModel();

        CatalogPresenter.Component component = DaggerService.getComponent(CatalogPresenter.Component.class);
        if (component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(CatalogPresenter.Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mCatalogModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }

    }

    @Override
    public void clickOnBuyButton(int position) {


        if (getView() != null) {

            if (checkUserAuth()) {
                getRootView().showMessage("Товар " + mProductList.get(position).getProductName() + " успешно добавлен в корзину.");
//                getView().showAddToCartMessage(mProductList.get(position));
            } else {
                getRootView().showAuthScreen();
            }

        }

    }

    public IRootView getRootView() {

        return mRootPresenter.getView();
    }

    @Override
    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }

    //region    ========================== DI ==========================

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = CatalogPresenter.Module.class)
    @CatalogScope
    public interface Component {

        void inject(CatalogPresenter catalogPresenter);
    }


    private CatalogPresenter.Component createDaggerComponent() {

        return DaggerCatalogPresenter_Component.builder()
                .rootComponent(DaggerService.getComponent(RootActivity.RootComponent.class))
                .module(new CatalogPresenter.Module())
                .build();
    }

    //endregion ========================== DI ==========================

}
