package com.example.anray.buyingapp.di.modules;

import com.example.anray.buyingapp.data.managers.DataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by anray on 16.12.2016.
 */
@Module
public class ModelModule {

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
