package com.example.anray.buyingapp.mvp.presenters;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anray on 01.11.2016.
 */

public class ProductPresenterFactory {
    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<>();

    private static void registerPresenter(ProductDTO product, ProductPresenter presenter) {
        sPresenterMap.put(String.valueOf(product.getId()), presenter);
    }

    public static ProductPresenter getInstance(ProductDTO product) {
        ProductPresenter presenter = sPresenterMap.get(String.valueOf(product.getId()));
        if (presenter == null) {
            presenter = ProductPresenter.newInstance(product);
            registerPresenter(product, presenter);
        }
        return presenter;
    }
}
