package com.example.anray.buyingapp;

import android.app.Application;

import com.example.anray.buyingapp.di.components.AppComponent;
import com.example.anray.buyingapp.di.components.DaggerAppComponent;
import com.example.anray.buyingapp.di.modules.AppModule;
import com.example.anray.buyingapp.di.modules.PicassoCacheModule;
import com.example.anray.buyingapp.di.modules.RootModule;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.mortar.ScreenScoper;
import com.example.anray.buyingapp.ui.activities.DaggerRootActivity_RootComponent;
import com.example.anray.buyingapp.ui.activities.RootActivity;

import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

/**
 * Created by anray on 23.10.2016.
 */

public class MvpApplication extends Application {

    //    public static SharedPreferences sSharedPreferences;
    private static AppComponent sAppComponent;

//    public static Context sContext;

    private MortarScope mRootScope;
    private RootActivity.RootComponent mRootActivityRootComponent;
    private MortarScope mRootActivityScope;


    //    public static Context getContext() {
//        return sContext;
//    public static SharedPreferences getSharedPreferences() {
//        return sSharedPreferences;
    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @Override
    public Object getSystemService(String name) {
        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        createAppComponent();
        createRootActivityComponent();

        mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, sAppComponent)
                .build("Root");

        mRootActivityScope = mRootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, mRootActivityRootComponent)
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(mRootScope);
        ScreenScoper.registerScope(mRootActivityScope);

       /* sContext = getApplicationContext();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);*/
    }

//    }


//    }

    private void createAppComponent() {

        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .build();

    }

    private void createRootActivityComponent() {

        mRootActivityRootComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(sAppComponent)
                .rootModule(new RootModule())
                .picassoCacheModule(new PicassoCacheModule())
                .build();
    }
}
