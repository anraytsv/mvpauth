package com.example.anray.buyingapp.di.components;

import com.example.anray.buyingapp.di.modules.PicassoCacheModule;
import com.example.anray.buyingapp.di.scopes.RootScope;
import com.squareup.picasso.Picasso;

import dagger.Component;

/**
 * Created by anray on 16.12.2016.
 */
@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {

    Picasso getPicasso();
}
