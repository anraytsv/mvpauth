package com.example.anray.buyingapp.mvp.models;

import com.example.anray.buyingapp.data.storage.DTO.ProductDTO;

import java.util.List;

/**
 * Created by anray on 02.11.2016.
 */

public class CatalogModel extends AbstractModel {


    public CatalogModel() {
    }

    public List<ProductDTO> getProductList(){
        return mDataManager.getProductList();
    }

    public boolean isUserAuth(){
        return mDataManager.isAuthUser();
    }

}
