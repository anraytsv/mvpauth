package com.example.anray.buyingapp.di.components;

import com.example.anray.buyingapp.di.modules.ModelModule;
import com.example.anray.buyingapp.mvp.models.AbstractModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by anray on 16.12.2016.
 */
@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
