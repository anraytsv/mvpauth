package com.example.anray.buyingapp.ui.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.example.anray.buyingapp.R;

/**
 * Created by anray on 13.11.2016.
 */

public class AspectRatioImageView extends ImageView {


    private static final float DEFAULT_ASPECT_RATIO = 1.73f;
    private final float mAspectRatio;

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        a.recycle();



    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int newWidth;
        int newHeight;

       /* newWidth = getMeasuredWidth();
        newHeight = (int) (newWidth/mAspectRatio);*/

        newHeight = getMeasuredHeight();
        newWidth = (int) (newHeight * mAspectRatio);
        setMeasuredDimension(newWidth,newHeight);

    }
}

