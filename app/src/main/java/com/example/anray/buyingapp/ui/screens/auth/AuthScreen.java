package com.example.anray.buyingapp.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.anray.buyingapp.R;
import com.example.anray.buyingapp.di.scopes.AuthScope;
import com.example.anray.buyingapp.di.scopes.DaggerService;
import com.example.anray.buyingapp.flow.AbstractScreen;
import com.example.anray.buyingapp.flow.Screen;
import com.example.anray.buyingapp.mvp.models.AuthModel;
import com.example.anray.buyingapp.mvp.presenters.IAuthPresenter;
import com.example.anray.buyingapp.mvp.presenters.RootPresenter;
import com.example.anray.buyingapp.mvp.views.IRootView;
import com.example.anray.buyingapp.ui.activities.RootActivity;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Created by anray on 06.01.2017.
 */

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCurrentState = 1;

    public int getCurrentState() {
        return mCurrentState;
    }

    public void setCurrentState(int currentState) {
        mCurrentState = currentState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentRootComponent) {
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentRootComponent)
                .module(new Module())
                .build();
    }

    //region    ========================== DI ==========================


    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        AuthPresenter providePresenter() {
            return new AuthPresenter();
        }

        @Provides
        @AuthScope
        AuthModel provideAuthModel() {
            return new AuthModel();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AuthScope
    public interface Component {
        void inject(AuthPresenter presenter);

        void inject(AuthView view);
    }

    //endregion ========================== DI ==========================


    //region    ========================== Presenter ==========================

    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {

        @Inject
        RootPresenter mRootPresenter;

        @Inject
        AuthModel mAuthModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if (getView() != null) {
                if (checkUserAuth()) {
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }

        @Override
        public void clickOnLogin() {

            if (getView() != null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().setCurrentState(AuthView.LOGIN_STATE);

                } else {
                    // TODO: 07.01.2017 auth user
//                    mAuthModel.loginUser(getView().getUserEmail(), getView().getUserPassword());
                    //stub message
                    if (getView().checkFieldsSetAndClearErrors()) {
                        getRootView().showMessage("Запрос на авторизацию отправлен");
                    }
                }
            }


        }

        @Override
        public void clickOnFb() {

            if (getRootView() != null) {
                getRootView().showMessage("clickOnFb");
            }

        }

        @Override
        public void clickOnVk() {

            if (getRootView() != null) {
                getRootView().showMessage("clickOnVk");
            }

        }

        @Override
        public void clickOnTwitter() {

            if (getRootView() != null) {
                getRootView().showMessage("clickOnTwitter");
            }

        }

        @Override
        public void clickOnShowCatalog() {

            if (getRootView() != null) {
//            getRootView().showMessage("Показать Каталог");

                // TODO: 31.10.2016 if update data complete show catalog screen
//            getView().showCatalogScreen();
                getRootView().showCatalogScreen();
            }

        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }
    }
    //endregion ========================== Presenter ==========================
}
