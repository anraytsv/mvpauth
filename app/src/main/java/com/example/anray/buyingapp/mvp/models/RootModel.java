package com.example.anray.buyingapp.mvp.models;

/**
 * Created by anray on 17.12.2016.
 */
public class RootModel extends AbstractModel {


    public void clearToken() {
        mDataManager.getPreferencesManager().clearToken();
    }

    public boolean isUserAuth(){

       return mDataManager.isAuthUser();

    }
}
