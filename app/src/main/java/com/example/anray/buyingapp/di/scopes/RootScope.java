package com.example.anray.buyingapp.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by anray on 17.12.2016.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RootScope {
}
