package com.example.anray.buyingapp.di.components;

import com.example.anray.buyingapp.data.managers.DataManager;
import com.example.anray.buyingapp.di.modules.LocalModule;
import com.example.anray.buyingapp.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by anray on 16.12.2016.
 */
@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {

    void inject(DataManager dataManager);


}
